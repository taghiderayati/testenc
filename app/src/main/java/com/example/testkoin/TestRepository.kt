package com.example.testkoin

import com.example.testkoin.data.LocalData
import com.example.testkoin.data.Response
import com.example.testkoin.remote.ApiAll
import setNetWork

class TestRepository(val mApiClient:ApiAll,val localData : LocalData) {

    suspend fun getInitApi(callBack: (Response<InitModel>) -> Unit) {
        setNetWork(callBack = callBack) {
            mApiClient.getInit()
        }
    }



}