package com.example.testkoin.di

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber

class MainApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@MainApplication)
            loadKoinModules(listOf(
                sharedPreferencesModule,
                NetworkModule, ApiServiceModule, viewModelModule, RepositoryModule
            ))
            koin.createRootScope()
        }
    }
}