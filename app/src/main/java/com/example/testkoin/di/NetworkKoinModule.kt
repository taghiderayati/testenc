package com.example.testkoin.di


import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import timber.log.Timber
import java.util.concurrent.TimeUnit

val NetworkModule = module {
    single { httpLoggingInterceptor() }
    single { okHttpClient(get()) }
}

private fun okHttpClient(
    httpLoggingInterceptor: HttpLoggingInterceptor
): OkHttpClient {

    return OkHttpClient.Builder()
        .addInterceptor(httpLoggingInterceptor)
        .readTimeout(30, TimeUnit.SECONDS)
        .writeTimeout(30, TimeUnit.SECONDS)
        .connectTimeout(30, TimeUnit.SECONDS)
        .build()
}


fun httpLoggingInterceptor(): HttpLoggingInterceptor {
    return HttpLoggingInterceptor { message ->
        Timber.log(Timber.treeCount(), message)
        Timber.tag("Network").e(message)
    }.apply {
        level = HttpLoggingInterceptor.Level.BODY
    }
}