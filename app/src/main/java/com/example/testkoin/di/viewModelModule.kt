package com.example.testkoin.di

import com.example.testkoin.TestViewModel
import org.koin.dsl.module


val viewModelModule= module {
   single  { TestViewModel(get()) }
}