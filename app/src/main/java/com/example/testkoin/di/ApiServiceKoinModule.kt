package com.example.testkoin.di

import com.example.testkoin.di.DataUrl.BASE_URL
import com.example.testkoin.remote.ApiAll
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object DataUrl{
    var BASE_URL = "https://www.passwordrandom.com/"
}

val ApiServiceModule= module {

    single {
        provideGson()
    }

    single {
        retrofit(get(),get() )
    }

    single {
        get<Retrofit>().create(ApiAll::class.java)
    }
}


private fun provideGson(): Gson = GsonBuilder().create()

private fun retrofit(gson: Gson, client: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .client(client)
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()



}