package com.example.testkoin.di

import android.content.Context
import android.content.SharedPreferences
import com.example.testkoin.data.LocalData
import com.example.testkoin.data.SharedPrefsHelper
import org.koin.dsl.module

val sharedPreferencesModule = module {
    single {
        sharedPreferences(get())
    }

    factory {  SharedPrefsHelper(get(),get()) }
    factory {  LocalData(get()) }

}

fun sharedPreferences(mContext: Context): SharedPreferences {
    return mContext.getSharedPreferences("MTD", Context.MODE_PRIVATE)
}