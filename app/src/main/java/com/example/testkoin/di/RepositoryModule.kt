package com.example.testkoin.di

import com.example.testkoin.TestRepository
import org.koin.dsl.module

val RepositoryModule= module {
    single {
        TestRepository(get(),get())
    }
}