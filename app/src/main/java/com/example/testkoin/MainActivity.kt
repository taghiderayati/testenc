package com.example.testkoin

import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import com.example.testkoin.data.Response
import com.example.testkoin.utilty.observe
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.concurrent.Executor

class MainActivity : AppCompatActivity() {

    private val testViewModel by viewModel<TestViewModel>()


    private lateinit var executor: Executor
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var promptInfo: BiometricPrompt.PromptInfo

    var isFlagAuth = false

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        observe(testViewModel.getInitApi()) {
            when (it?.status) {
                Response.Status.LOADING -> Toast.makeText(
                    this@MainActivity,
                    "Loading Data ....", Toast.LENGTH_SHORT
                )
                    .show()
                Response.Status.ERROR -> Toast.makeText(
                    this@MainActivity,
                    "Error Fetch Data ....", Toast.LENGTH_SHORT
                )
                    .show()
                Response.Status.SUCCESS -> {
                    if (!isFlagAuth) {
                        biometricPrompt.authenticate(promptInfo)
                    }
                    Toast.makeText(
                        this@MainActivity,
                        "Data is Ready ....", Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

        btn.setOnClickListener {
            if (isFlagAuth) {
                edt.text?.let {
                    val enc = testViewModel.encryptWithToken(it.toString().trim())
                    Toast.makeText(this, "Data Encrypt ....=> $enc", Toast.LENGTH_LONG).show()

                    Handler().postDelayed({
                        val dec = testViewModel.decryptWithToken(enc)
                        Toast.makeText(this, "Data Decrypt ....=> $dec", Toast.LENGTH_LONG).show()
                    }, 3000)

                }

            } else {
                biometricPrompt.authenticate(promptInfo)
            }
        }

        setInitBiometric()

    }

    private fun setInitBiometric() {
        executor = ContextCompat.getMainExecutor(this)
        biometricPrompt = BiometricPrompt(this, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(
                    errorCode: Int,
                    errString: CharSequence
                ) {
                    super.onAuthenticationError(errorCode, errString)
                    isFlagAuth = false
                    Toast.makeText(
                        this@MainActivity,
                        "Authentication error: $errString", Toast.LENGTH_SHORT
                    )
                        .show()
                }

                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                    isFlagAuth = true
                    super.onAuthenticationSucceeded(result)
                    Toast.makeText(
                        this@MainActivity,
                        "Authentication succeeded!", Toast.LENGTH_SHORT
                    )
                        .show()
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    isFlagAuth = false
                    Toast.makeText(
                        this@MainActivity, "Authentication failed",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
            })

        promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("Biometric login for my app")
            .setSubtitle("Log in using your biometric credential")
            .setNegativeButtonText("Use account password")
            .build()
    }
}