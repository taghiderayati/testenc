package com.example.testkoin.remote

import com.example.testkoin.InitModel
import retrofit2.http.GET

interface ApiAll {
    @GET("query?command=password&format=json&count=1")
    suspend fun getInit(): InitModel
}