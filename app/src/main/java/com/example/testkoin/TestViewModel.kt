package com.example.testkoin

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModel
import com.example.testkoin.data.Constant.Companion.KEY_ALIAS_MTD
import com.example.testkoin.data.Response
import com.example.testkoin.utilty.LiveEvent
import com.example.testkoin.utilty.SymmetricCryptography
import execute

class TestViewModel(val testRepository: TestRepository) : ViewModel() {

    var data = "1"


    @RequiresApi(Build.VERSION_CODES.O)
    fun getInitApi() = LiveEvent<Response<InitModel>>().apply {
        if (testRepository.localData.getToken() == null) {
            execute {
                testRepository.getInitApi {
                    if (it.status == Response.Status.SUCCESS) {
                        generateAssymetricKey(KEY_ALIAS_MTD)
                        val keyEn = encrypt(KEY_ALIAS_MTD, it.data!!.char[0])
                        testRepository.localData.setToken(keyEn)
                    }
                    this@apply.postValue(it)
                }
            }
        }else{
            this.postValue(Response.success())
        }
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun generateAssymetricKey(alias: String) {
        SymmetricCryptography().generateSymmetricKey(alias)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun encrypt(alias: String, username: String): String {
        return SymmetricCryptography().encryptDataAsymmetric(alias, username)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun decrypt(alias: String, enString: String): String {
        return SymmetricCryptography().decryptDataAsymmetric(alias, enString)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun encryptWithToken(username: String): String {
        val token = decrypt(KEY_ALIAS_MTD, testRepository.localData.getToken()!!)
        generateAssymetricKey(token)
        return encrypt(token, username)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun decryptWithToken(username: String): String {
        val token = decrypt(KEY_ALIAS_MTD, testRepository.localData.getToken()!!)
        return decrypt(token, username)
    }

}