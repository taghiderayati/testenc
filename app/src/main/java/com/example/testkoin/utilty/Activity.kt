/*
 *  Created by M.Taghi Derayati (MTD) on 2/29/20 10:51 PM
 */

package com.example.testkoin.utilty

import android.content.Context
import android.os.Build
import android.view.View
import androidx.annotation.ColorRes
import androidx.annotation.NonNull
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider



fun <T : Any, L : LiveData<T>> LifecycleOwner.observe(liveData: L, body: (T?) -> Unit) {
    liveData.observe(this) {
        body(it)

    }
}
