package com.example.testkoin.utilty

import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import androidx.annotation.RequiresApi
import java.lang.Exception
import java.nio.charset.Charset
import java.security.KeyStore
import java.util.*
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.spec.IvParameterSpec
class SymmetricCryptography {
    val keystore: KeyStore = initializeKeystore()

    fun initializeKeystore(): KeyStore {
        val ks = KeyStore.getInstance("AndroidKeyStore")
        ks.load(null)
        return ks
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun generateSymmetricKey(alias: String) {
        val generator = KeyGenerator.getInstance(
            KeyProperties.KEY_ALGORITHM_AES,
            "AndroidKeyStore"
        )
        val generatorSpec = KeyGenParameterSpec.Builder(
            alias,
            KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
        )
            .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
            .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)

        generator.init(generatorSpec.build())
        generator.generateKey()
    }
    fun getAsymmetricKey(alias: String): SecretKey? {
        try {
            return keystore.getKey(alias, CharArray(0)) as SecretKey
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun encryptDataAsymmetric(alias: String, data: String): String {
        val key = getAsymmetricKey(alias)
        val plainTextByteArray = data.toByteArray(Charset.defaultCharset())

        val cipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
        cipher.init(Cipher.ENCRYPT_MODE, key)
        val cipherText = cipher.doFinal(plainTextByteArray)
        return Base64.getEncoder()
            .encodeToString(cipherText) +
                "," +
                Base64.getEncoder().encodeToString(cipher.iv)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun decryptDataAsymmetric(alias: String, data: String): String {
        val key = getAsymmetricKey(alias)
        val parts = data.split(",")
        val plainTextByteArray = Base64.getDecoder().decode(parts[0])

        val iv = Base64.getDecoder().decode(parts[1])

        val cipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
        cipher.init(Cipher.DECRYPT_MODE, key, IvParameterSpec(iv))
        val cipherText = cipher.doFinal(plainTextByteArray)

        return cipherText.toString(Charset.defaultCharset())
    }
}