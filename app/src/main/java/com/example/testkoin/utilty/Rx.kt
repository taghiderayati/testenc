
import com.example.testkoin.data.Response
import kotlinx.coroutines.*
import retrofit2.HttpException
import kotlin.coroutines.CoroutineContext
import retrofit2.Response as ResponseRetrofit

/*
 *  Created by M.Taghi Derayati (MTD) on 4/26/20 4:35 PM
 */

var backgroundContext: CoroutineContext = Dispatchers.IO
var foregroundContext: CoroutineContext = Dispatchers.Main
private var parentJob: Job = Job()


fun execute(
    block: suspend CoroutineScope.() -> Unit
) {
    val handler = CoroutineExceptionHandler { _, e ->

    }
    CoroutineScope(foregroundContext + handler).launch {
        withContext(backgroundContext) {
            block.invoke(this)
        }
    }
}


suspend fun <T> setNetWork(
    callBack: (Response<T>) -> Unit,
    block: suspend () -> T
) {
    callBack(Response.loading())
    try {
        val data = block.invoke()
        if (data is ResponseRetrofit<*>) {
            val datres = data as ResponseRetrofit<*>
            if (!datres.isSuccessful)
                throw HttpException(data)
        }
        callBack(Response.success(data)!!)
    } catch (e: Exception) {
        callBack(Response.error(e.message))
    }

}
