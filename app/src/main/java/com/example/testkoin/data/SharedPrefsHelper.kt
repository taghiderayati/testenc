package com.example.testkoin.data

import android.content.SharedPreferences
import com.google.gson.Gson

class SharedPrefsHelper  constructor(
     var mSharedprefrense: SharedPreferences,
     var gson: Gson
) {


    fun put(key: String?, value: String) {
            mSharedprefrense.edit().putString(key.toString(), value).apply()
    }

    fun put(key: String, value: Int) {
        mSharedprefrense.edit().putInt(key, value).apply()
    }

    fun put(key: String, value: Float) {
        mSharedprefrense.edit().putFloat(key, value).apply()
    }

    fun put(key: String, value: Boolean) {
        mSharedprefrense.edit().putBoolean(key, value).apply()
    }

    fun get(key: String?, defaultValue: String?): String? {
       return mSharedprefrense.getString(key, defaultValue)
    }

    fun get(key: String?): Int? {
        return mSharedprefrense.getInt(key, 0)
    }

    fun get(key: String, defaultValue: Int): Int? {
        return mSharedprefrense.getInt(key, defaultValue)
    }

    fun get(key: String, defaultValue: Float): Float? {
        return mSharedprefrense.getFloat(key, defaultValue)
    }

    fun get(key: String, defaultValue: Boolean): Boolean? {
        return mSharedprefrense.getBoolean(key, defaultValue)
    }

    fun putObject(obj: Any?) {
        if (obj == null) {
            throw IllegalArgumentException("object is null")
        }
        try {
            put(obj.javaClass.canonicalName, gson.toJson(obj))
        } catch (e: Exception) {
        }
    }

    fun <T> getObject(a: Class<T>): T? {
        val json: String? = get(a.canonicalName, null)
        try {
            return gson.fromJson(json, a)
        } catch (e: Exception) {
            throw IllegalArgumentException("Object storaged with key " + a.canonicalName + " is instanceof other class")
        }

    }

    fun deleteSavedData(key: String) {
        mSharedprefrense.edit().remove(key).apply()
    }

    fun <T>deleteSavedData(a: Class<T>) {
        mSharedprefrense.edit().remove(a.canonicalName).apply()
    }

    fun clearAll() {
        mSharedprefrense.edit().clear().apply()
    }


}
