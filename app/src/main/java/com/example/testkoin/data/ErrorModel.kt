package com.eco.citizen.core.data


import com.google.gson.annotations.SerializedName

data class ErrorModel(@SerializedName("error")
                      val error: String = "",
                      @SerializedName("message")
                      val message: String = "",
                      @SerializedName("statusCode")
                      val statusCode: Int = 0)


