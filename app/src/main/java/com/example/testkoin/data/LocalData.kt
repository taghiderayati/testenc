package com.example.testkoin.data

import com.example.testkoin.data.Constant.Companion.KEY_TOKEN


class LocalData (var sharedPrefsHelper: SharedPrefsHelper) {



    fun setToken(token: String) {
        sharedPrefsHelper.put(KEY_TOKEN, token)
    }

    fun getToken(): String? {
        sharedPrefsHelper.get(KEY_TOKEN, null).apply {
            return if (this == null || this == "") {
                null
            } else {
                "$this"
            }
        }
    }

}
